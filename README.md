# react-native-mht-printing

Library for MHT printing

## Installation

In **package.json**

```json
{
  "dependencies": {
    "react-native-mht-printing": "git+https://gitlab.com/vansokchea366/react-native-mht-printing#master"
  }
}
```

## Android

In MainApplication.java

```java
import android.os.Handler;
import java.util.concurrent.ExecutorService;
import com.reactnativemhtprinting.utils.ThreadPool;

public class MainApplication extends Application implements ReactApplication {
  ExecutorService cachedThreadPool = null;
  private Handler handler = new Handler();

  public ThreadPool.ThreadPoolBinder binder = new ThreadPool.ThreadPoolBinder() {
    @Override
    public ExecutorService getCachedThreadPool() {
      return cachedThreadPool;
    }

    @Override
    public Handler getHandler() {
      return handler;
    }
  };

  @Override
  public void onCreate() {
    ...

    cachedThreadPool = Executors.newCachedThreadPool();
    ThreadPool.getSharedInstance().registerBinder(binder);
  }
}

```

## Usage

Check Example [HERE](./example/src/App.tsx)

## Contributing

See the [contributing guide](CONTRIBUTING.md) to learn how to contribute to the repository and the development workflow.

## License

MIT
