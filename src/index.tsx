import { createRef } from 'react';
import { NativeModules, NativeEventEmitter, Alert } from 'react-native';

export { default as PrinterConnectionModal } from './components/PrinterConnectionModal'

const { MHTPrinting } = NativeModules;

export const printerConnectionModalRef = createRef<{
    show: (callback?: () => void) => void
}>();

export type BluetoothDevice = {
    name: string,
    address: string
}

export const getPairedBluetoothDevices: () => Promise<
    BluetoothDevice[]
> = MHTPrinting.getPairedBluetoothDevices;

export const connectBluetoothDevice: (deviceAddress: string) => void = MHTPrinting.connectBluetoothDevice;

export const connectPairedBluetoothDevice: (index: number) => void = MHTPrinting.connectPairedBluetoothDevice;

export const stopSearchBluetoothDevices: () => void = MHTPrinting.stopSearchBluetoothDevices;

export const getConnectedBluetoothDevice: (callback: (deviceName: string, deviceAddress: string) => void) => void = MHTPrinting.getConnectedBluetoothDevice;

export const searchBluetoothDevices: () => void = MHTPrinting.searchBluetoothDevices;

export const subscribeBluetoothDeviceSearchListener = (
    listener: (event: {
        name: string,
        address: string,
    }) => void,
): (() => void) => {
    const eventEmitter = new NativeEventEmitter(MHTPrinting);
    const eventListener = eventEmitter.addListener(
        'onBluetoothDeviceFound',
        listener,
    );
    return () => eventListener?.remove();
};

export const subscribeBluetoothDeviceConnectedListener = (
    listener: (event: {
        name: string,
        address: string,
    }) => void,
): (() => void) => {
    const eventEmitter = new NativeEventEmitter(MHTPrinting);
    const eventListener = eventEmitter.addListener(
        'onBluetoothDeviceConnected',
        listener,
    );
    return () => eventListener?.remove();
};

export const DefaultPrintingOptions = {
    x: 30,
    y: 40,
    width: 58,
    height: 70,
};

export const print = (
    base64Image: string,
    options: {
        x: number,
        y: number,
        width: number,
        height: number,
    },
): Promise<'Printing' | 'NoBluetoothDeviceConnected'> => {
    return MHTPrinting.print(base64Image, options);
};

export const tryPrint = (base64Image: string,
    options: {
        printingCanvas: {
            x: number,
            y: number,
            width: number,
            height: number,
        },
        printingConfigDialog?: {
            title: string,
            message: string,
            cancelButtonTitle: string,
            okButtonTitle: string,
        }
    }): Promise<'Printing' | 'NoBluetoothDeviceConnected'> => {
    return new Promise((resolve, reject) => {
        getConnectedBluetoothDevice((deviceName: string, deviceAddress: string) => {

            if (deviceAddress && deviceName) {
                print(
                    base64Image,
                    options.printingCanvas,
                )
                    .then(resolve)
                    .catch(reject);
            } else {
                printerConnectionModalRef.current?.show(() => {
                    Alert.alert(options?.printingConfigDialog?.title ?? "Print", options?.printingConfigDialog?.message ?? "Are you sure to print?", [
                        {
                            text: options?.printingConfigDialog?.cancelButtonTitle ?? 'Cancel',
                        },
                        {
                            text: options?.printingConfigDialog?.okButtonTitle ?? 'OK',
                            onPress: () => {
                                print(
                                    base64Image,
                                    options.printingCanvas,
                                )
                                    .then(resolve)
                                    .catch(reject);
                            }
                        }
                    ])
                });
            }
        });
    });
};