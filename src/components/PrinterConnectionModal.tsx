import React, { useImperativeHandle } from 'react';
import {
  Modal,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native';
import * as MHTPrintingModule from '../index';

const PrinterConnectionModal = React.forwardRef(
  (
    props: {
      pairedSectionTitle?: string;
      unpairedSectionTitle?: string;
      connectedSectionTitle?: string;
      closeButtonTitle?: string;
    },
    ref
  ) => {
    const callbackRef = React.useRef<() => void>();
    const imageDataRef = React.useRef<string>();
    const [isVisible, setIsVisible] = React.useState(false);
    const [pairedDevices, setPairedDevices] = React.useState<
      MHTPrintingModule.BluetoothDevice[]
    >([]);
    const [devices, setDevices] = React.useState<
      MHTPrintingModule.BluetoothDevice[]
    >([]);
    const [connectedDevice, setConnectedDevice] =
      React.useState<MHTPrintingModule.BluetoothDevice | null>(null);

    const unpairedDevices = React.useMemo(() => {
      return devices
        .filter((d) => !pairedDevices.some((p) => p.address == d.address))
        .map((d) => ({ ...d, unpaired: true }));
    }, [pairedDevices, devices]);

    const getConnectedDevice = () => {
      MHTPrintingModule.getConnectedBluetoothDevice(
        (deviceName: string, deviceAddress: string) => {
          if (deviceAddress) {
            setConnectedDevice({
              name: deviceName,
              address: deviceAddress,
            });

            console.log('Connected device: ', deviceName, deviceAddress);
          }
        }
      );
    };

    React.useEffect(() => {
      getConnectedDevice();

      const unsubscribeSearchListener =
        MHTPrintingModule.subscribeBluetoothDeviceSearchListener((device) => {
          setDevices((lastDevices) => [...lastDevices, device]);
          console.log('Device: ', device);
        });
      const unsubscribeConnectedListener =
        MHTPrintingModule.subscribeBluetoothDeviceConnectedListener(() => {
          getConnectedDevice();
          callbackRef.current?.();
          setIsVisible(false);
          console.log('Connected!');
        });

      return () => {
        unsubscribeSearchListener?.();
        unsubscribeConnectedListener?.();
      };
    }, [callbackRef, setIsVisible]);

    useImperativeHandle(
      ref,
      () => ({
        print: (base64Image: string) => {
          setDevices([]);
          setConnectedDevice(null);
          setPairedDevices([]);
          imageDataRef.current = base64Image;
          setIsVisible(true);
        },
        hasConnectedDevice: () => !!connectedDevice,
        show: (callback: () => void) => {
          callbackRef.current = callback;
          setDevices([]);
          setConnectedDevice(null);
          setPairedDevices([]);
          setIsVisible(true);
        },
      }),
      [
        setIsVisible,
        connectedDevice,
        imageDataRef,
        setDevices,
        setConnectedDevice,
        setPairedDevices,
        callbackRef,
      ]
    );

    const renderItem = (
      device: MHTPrintingModule.BluetoothDevice,
      index: number
    ) => {
      const isConnected = connectedDevice?.address == device.address;

      return (
        <TouchableOpacity
          key={index}
          style={{
            padding: 10,
          }}
          onPress={() => {
            // MHTPrintingModule.stopSearchBluetoothDevices();
            MHTPrintingModule.connectBluetoothDevice(device.address);
          }}
        >
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Text>{device.name}</Text>
            {isConnected && (
              <Text
                style={{
                  fontSize: 12,
                  color: 'grey',
                }}
              >
                Connected
              </Text>
            )}
          </View>
          <Text
            style={{
              fontSize: 12,
              color: 'grey',
              marginTop: 4,
            }}
          >
            {device.address}
          </Text>
        </TouchableOpacity>
      );
    };

    const renderSectionTitle = (title: string) => {
      return (
        <View
          style={{
            backgroundColor: '#c8c2bc',
            padding: 10,
          }}
        >
          <Text
            style={{
              fontWeight: 'bold',
            }}
          >
            {title}
          </Text>
        </View>
      );
    };

    const onShow = () => {
      MHTPrintingModule.getPairedBluetoothDevices()
        .then((devices: MHTPrintingModule.BluetoothDevice[]) => {
          setPairedDevices(devices);
          MHTPrintingModule.searchBluetoothDevices();
        })
        .catch(console.log);
    };

    const renderCloseButton = () => (
      <TouchableOpacity
        style={{
          alignItems: 'center',
          paddingVertical: 15,
          margin: 15,
          borderRadius: 10,
          borderColor: 'red',
          borderWidth: StyleSheet.hairlineWidth,
        }}
        onPress={() => {
          setIsVisible(false);
        }}
      >
        <Text
          style={{
            color: 'red',
          }}
        >
          {props.closeButtonTitle ?? 'Close'}
        </Text>
      </TouchableOpacity>
    );

    return (
      <Modal
        visible={isVisible}
        transparent
        animationType="fade"
        onShow={onShow}
      >
        <View
          style={{
            flex: 1,
            backgroundColor: 'rgba(0,0,0,0.5)',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <View
            style={{
              backgroundColor: '#fff',
              height: '70%',
              width: '80%',
              borderRadius: 10,
              shadowColor: '#000',
              shadowOffset: {
                width: 0,
                height: 2,
              },
              shadowOpacity: 0.23,
              shadowRadius: 2.62,
              elevation: 4,
            }}
          >
            <Text
              style={{
                alignSelf: 'center',
                fontSize: 17,
                fontWeight: 'bold',
                marginVertical: 10,
              }}
            >
              Choose Printer to Print
            </Text>
            <ScrollView
              style={{
                flex: 1,
              }}
            >
              {Platform.OS == 'android' ? (
                <>
                  {renderSectionTitle(
                    props.connectedSectionTitle ?? 'Connected Device'
                  )}
                  {connectedDevice ? (
                    renderItem(connectedDevice, -1)
                  ) : (
                    <Text
                      style={{
                        color: 'grey',
                        margin: 10,
                      }}
                    >
                      No Connected Device
                    </Text>
                  )}
                  {renderSectionTitle(
                    props.pairedSectionTitle ?? 'Paired Devices'
                  )}
                  {pairedDevices.map(renderItem)}
                </>
              ) : null}
              {renderSectionTitle(
                props.unpairedSectionTitle ??
                  Platform.select({
                    android: 'Unpaired Devices',
                    ios: 'Devices',
                    default: '',
                  })
              )}
              {unpairedDevices.map(renderItem)}
            </ScrollView>
            {renderCloseButton()}
          </View>
        </View>
      </Modal>
    );
  }
);

export default PrinterConnectionModal;
