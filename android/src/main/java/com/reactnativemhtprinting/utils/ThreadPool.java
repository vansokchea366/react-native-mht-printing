package com.reactnativemhtprinting.utils;

import android.os.Handler;

import java.util.concurrent.ExecutorService;

public class ThreadPool {
  static private ThreadPool sharedInstance = new ThreadPool();

  private ThreadPoolBinder binder;

  private ThreadPool() {
  }

  public void registerBinder(ThreadPoolBinder binder) {
    this.binder = binder;
  }

  public ExecutorService getCachedThreadPool() {
    return binder.getCachedThreadPool();
  }

  public Handler getHandler() {
    return binder.getHandler();
  }

  static public ThreadPool getSharedInstance() {
    return sharedInstance;
  }

  public interface ThreadPoolBinder {
    ExecutorService getCachedThreadPool();
    Handler getHandler();
  }
}
