package com.reactnativemhtprinting;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.util.Log;

import androidx.annotation.Nullable;

import com.facebook.react.bridge.ActivityEventListener;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.LifecycleEventListener;
import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.WritableArray;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.module.annotations.ReactModule;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.reactnativemhtprinting.manager.PrintfManager;
import com.reactnativemhtprinting.manager.SharedPreferencesManager;
import com.reactnativemhtprinting.utils.ThreadPool;
import com.reactnativemhtprinting.utils.Util;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@ReactModule(name = MhtPrintingModule.NAME)
public class MhtPrintingModule extends ReactContextBaseJavaModule implements LifecycleEventListener, ActivityEventListener {
  public static final String NAME = "MHTPrinting";

  static public String TAG = "MHTPrintingModule";
  private static int REQUEST_ENABLE_BT = 2;

  private ReactApplicationContext mContext;
  private PrintfManager printfManager;
  private PrintfManager.BluetoothChangLister bluetoothChangLister;
  private BluetoothAdapter mBluetoothAdapter;
  private boolean isRegister;
  private ArrayList<BluetoothDevice> bluetoothDeviceArrayList;
  private boolean initialized = false;

  private BroadcastReceiver mReceiver = new BroadcastReceiver() {
    public void onReceive(Context context, Intent intent) {
      String action = intent.getAction();
      Log.d(TAG, "Received: " + action);

      //找到设备
      if (BluetoothDevice.ACTION_FOUND.equals(action)) {
        BluetoothDevice device = intent
          .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
        if (judge(device, MhtPrintingModule.this.bluetoothDeviceArrayList))
          return;
        MhtPrintingModule.this.bluetoothDeviceArrayList.add(device);
        WritableMap params = Arguments.createMap();
        params.putString("name", device.getName());
        params.putString("address", device.getAddress());
        sendEvent(MhtPrintingModule.this.mContext, "onBluetoothDeviceFound", params);

        Log.d(TAG, "Found device: " + device.getName());
      }
      //搜索完成
      else if (BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action)) {
        stopSearchBluetoothDevices();
      }
    }
  };

  public MhtPrintingModule(ReactApplicationContext reactContext) {
    super(reactContext);
    mContext = reactContext;

    bluetoothDeviceArrayList = new ArrayList<>();

    mContext.addLifecycleEventListener(this);
    mContext.addActivityEventListener(this);
  }

  @Override
  public String getName() {
    return NAME;
  }

  private void sendEvent(ReactContext reactContext,
                         String eventName,
                         @Nullable WritableMap params) {
    reactContext
      .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
      .emit(eventName, params);
  }


  @Override
  public void onHostResume() {
    if (!initialized) {
      mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

      printfManager = PrintfManager.getInstance(mContext);

      printfManager.setConnectSuccess(new PrintfManager.ConnectSuccess() {
        @Override
        public void success() {
          sendEvent(MhtPrintingModule.this.mContext, "onBluetoothDeviceConnected", Arguments.createMap());
        }
      });

      bluetoothChangLister = new PrintfManager.BluetoothChangLister() {
        @Override
        public void chang(String name, String address) {
//                tv_blue_list_name.setText(getString(R.string.name_colon) + name);
//                tv_blue_list_address.setText(getString(R.string.address_colon) + address);
        }
      };

      printfManager.addBluetoothChangLister(bluetoothChangLister);
      printfManager.defaultConnection();

      initialized = true;
    }
  }

  @Override
  public void onHostPause() {

  }

  @Override
  public void onHostDestroy() {

  }

  @Override
  public void onActivityResult(Activity activity, int requestCode, int resultCode, Intent data) {
    if (requestCode == REQUEST_ENABLE_BT) {
      if (resultCode == Activity.RESULT_OK) {
        mBluetoothAdapter.startDiscovery();
      }
    }
  }

  @Override
  public void onNewIntent(Intent intent) {

  }

  @ReactMethod
  public void stopSearchBluetoothDevices() {
    if (mReceiver != null && isRegister) {
      try {
        mContext.unregisterReceiver(mReceiver);
        isRegister = false;
      } catch (Exception e) {
      }
    }
    if (mBluetoothAdapter != null) {
      mBluetoothAdapter.cancelDiscovery();
    }
  }

  private boolean judge(BluetoothDevice device, List<BluetoothDevice> devices) {
    if (device == null || devices.contains(device)) {
      return true;
    }
//        //Filter 1536 stands for printer
//        int majorDeviceClass = device.getBluetoothClass().getMajorDeviceClass();
//        if(majorDeviceClass != 1536){
//            return true;
//        }
    return false;
  }

  @ReactMethod
  public void getConnectedBluetoothDevice(Callback callback) {
    String blueName = "";
    String blueAddress = "";

    if (printfManager.isConnect()) {
      blueName = SharedPreferencesManager.getBluetoothName(mContext);
      blueAddress = SharedPreferencesManager.getBluetoothAddress(mContext);
    }

    callback.invoke(blueName, blueAddress);
  }

  @ReactMethod
  public void getPairedBluetoothDevices(Promise promise) {
    ArrayList<BluetoothDevice> devices = new ArrayList<>();
    WritableArray params = Arguments.createArray();
    Set<BluetoothDevice> bondedDevices = mBluetoothAdapter.getBondedDevices();
    for (BluetoothDevice device : bondedDevices) {
      if (judge(device, devices))
        continue;

      WritableMap data = Arguments.createMap();
      data.putString("name", device.getName());
      data.putString("address", device.getAddress());

      params.pushMap(data);
      devices.add(device);
    }
    promise.resolve(params);
  }

  @ReactMethod
  public void searchBluetoothDevices() {
    bluetoothDeviceArrayList.clear();
    IntentFilter filter = new IntentFilter();
    filter.addAction(BluetoothDevice.ACTION_FOUND);
    filter.addAction(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
    mContext.registerReceiver(mReceiver, filter);
    isRegister = true;
    if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
      Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
      mContext.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT, null);
    } else {
      mBluetoothAdapter.startDiscovery();

      Log.d(TAG, "Search bluetooth devices");
    }
  }

  @ReactMethod
  public void connectBluetoothDevice(String address) {
    stopSearchBluetoothDevices();

    //进行配对
    ThreadPool.getSharedInstance().getCachedThreadPool().execute(new Runnable() {
      @Override
      public void run() {
        try {
          BluetoothDevice mDevice = MhtPrintingModule.this.mBluetoothAdapter.getRemoteDevice(address);
          MhtPrintingModule.this.printfManager.openPrinter(mDevice);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    });
  }

  @ReactMethod
  public void connectPairedBluetoothDevice(int index) {
    ThreadPool.getSharedInstance().getCachedThreadPool().execute(new Runnable() {
      @Override
      public void run() {
        MhtPrintingModule.this.printfManager.openPrinter(bluetoothDeviceArrayList.get(index));
      }
    });
  }

  @ReactMethod
  public void print(String base64ImageData, ReadableMap options, Promise promise) {
    Bitmap bitmap = Util.base64toBitmap(base64ImageData);

    int x = options.hasKey("x") ? options.getInt("x") : 0;
    int y = options.hasKey("y") ? options.getInt("y") : 0;
    int w = options.hasKey("width") ? options.getInt("width") : 55;
    int h = options.hasKey("height") ? options.getInt("height") : 70;

    printfManager.printf(x, y, w , h, bitmap, new PrintfManager.PrintListener() {
      @Override
      public void onPrintResult(String result) {
        if (result == PrintfManager.PRINTING) {
          promise.resolve(result);
        } else {
          promise.reject("Error", result);
        }
      }
    });
  }
}
