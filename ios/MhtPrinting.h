#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface MhtPrinting : RCTEventEmitter <RCTBridgeModule> {
    BOOL hasListeners;
}

+(void)attach;

@end
