//
//  BluetoothModel.h
//  MHTReceipt
//
//  Created by Mac on 2018/11/6.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>


NS_ASSUME_NONNULL_BEGIN

@interface BluetoothModel : NSObject

@property (strong,nonatomic)CBPeripheral * peripheral;

@end

NS_ASSUME_NONNULL_END
