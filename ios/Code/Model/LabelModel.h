//
//  LabelModel.h
//  DemoLabel
//
//  Created by Mac on 2018/12/8.
//  Copyright © 2018年 label printf demo. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface LabelModel : NSObject

@property (assign,nonatomic) CGFloat labelWidth;
@property (assign,nonatomic) CGFloat labelHeight;

@property (strong,nonatomic) UIImage *printfUIImage;

@end

NS_ASSUME_NONNULL_END
