//
//  MHTReceiptUtils.h
//  MHTReceipt
//
//  Created by Mac on 2018/10/10.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MHTReceiptUtils : NSObject

//@property static ;

//刷新
+(void)sendNotificationRefresh;

//更改BaseControl的位置
+(void)sendNotificationChangeIndex:(int) startIndex widthEndIndex:(int) endIndex;

//显示UIView
+(void)sendNotificationShowAttribute:(UIView *)uiView;

//关闭显示出来的UIView
+(void)sendNotificationDismissShowAttributeUIView;

//通知UIView 刷新所有的BaseControl宽高
+(void)sendNotificationInitBaseControlWH;

//保存
+(void)sendNotificationSaveModel;

//另存为
+(void)sendNotificationSaveAsModel;

//选择选择图片
+(void)sendNotificationSelectImageForControl:(NSObject *)baseControl;

+(void)sendNotificationInitUIScrollView;

+(void)sendNotificationPositionChange;

//打印机名字改变
+(void)sendNotificationPrintfNameChange:(NSString *)printfName;

//打印方式改变
+(void)sendNotificationPrintfTypeChange:(NSString *)typeName;

//打印的通知
+(void)sendNotificationPrintf;

//通知去改变打印类型
+(void)sendNotificationChangeType;

//上传模板
+(void)sendNotificationUploadTemplate;

/**
 * 通知打印机的指令模式已经改变：改变发生的情况是，主动去查询打印机发生的
 */
+(void)sendNotificationPrinterInstructChange;

/**
 * 通知打印机的纸张模式已经改变：改变发生的情况是，主动去查询打印机发生的
 */
+(void)sendNotificationPrinterPaperChange;

/**
 * 服务器上的国际化的配置表
 */
+(NSString *) getServerInter:(NSString *)text;

/**
 * 登录状态改变
 * 登录 状态通知做不同的通知转发
 * code == 0 登录成功
 * code == 1 登录失败
 */
+(void)sendNotificationLoginStateChange:(NSString *)msg withLoginState:(int)code;

//密码加密
+(NSString *)passwordEncry:(NSString *)password;

/**
 * 弹出加载框
 */
+(void)showLoading:(NSString *)text withUIView:(UIView *)parentView;

/**
 *隐藏加载框 根据UIView
 */
+(void)dismissLoading:(UIView *)parentView;

/**
 * 生成一个居中的，提示UILabel
 */
+(UILabel *)getTipsUILabel:(NSString *)text withStartY:(CGFloat)startY;

@end

NS_ASSUME_NONNULL_END
