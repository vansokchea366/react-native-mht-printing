//
//  LabelPrinterUtils.h
//  DemoLabel
//
//  Created by Mac on 2018/12/8.
//  Copyright © 2018年 label printf demo. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "LabelModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface LabelPrinterUtils : NSObject

/**
 * 把LabelModel 转化成 打印机可以识别的数据
 */
+(NSData *)toLabelNSData:(LabelModel *)labelModel number:(int)number;


@end

NS_ASSUME_NONNULL_END
