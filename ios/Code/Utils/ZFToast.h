//
//  ZFToast.h
//  bultoothText
//
//  Created by Mac on 2018/12/7.
//  Copyright © 2018年 Foshan New Media Technology Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZFToast : UIView

- (void)popUpToastWithMessage:(NSString *)message;

@end

NS_ASSUME_NONNULL_END
