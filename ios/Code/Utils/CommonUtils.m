//
//  CommonUtils.m
//  MHTReceipt
//
//  Created by Mac on 2018/10/9.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "CommonUtils.h"

#define SCREEN_W  [UIScreen mainScreen].bounds.size.width
#define SCREEN_H  [UIScreen mainScreen].bounds.size.height

#define TEXT_SIZE 19 

@implementation CommonUtils


/**
 * 将字节数组，拼接成16进制的小写字符
 */
+(NSString *)bytesToHexNSString:(NSData *)nsData{
    NSMutableString *text = [[NSMutableString alloc] initWithCapacity:5];
    Byte *bytes = (Byte *)[nsData bytes];
    for(int i = 0; i < [nsData length]; i++){
        [text appendString:[NSString stringWithFormat:@"%0x", bytes[i]]];
    }
    return text;
}

+(BOOL)judgeIsItpPoint:(CGRect)cgRect withCGPoint:(CGPoint)cgPoint{
    int startX = cgRect.origin.x;
    int startY = cgRect.origin.y;
    int endX = cgRect.origin.x + cgRect.size.width;
    int endY = cgRect.origin.y + cgRect.size.height;
    if(cgPoint.x > startX && cgPoint.x < endX && cgPoint.y > startY && cgPoint.y < endY){
        return true;
    }
    return false;
}

+(CGFloat)autoScreen{
    if(SCREEN_W == 320){
        return 0.85;
    }else if(SCREEN_W == 375){
        return 1.00;
    }else if(SCREEN_W == 414){
        return 1.15;
    }else {
        return 1.00;
    }
}

/**
 @method 获取指定宽度情况ixa，字符串value的高度
 @param value 待计算的字符串
 @param width 限制字符串显示区域的宽度
 @result float 返回的高度
 */
+ (float) getNSStringHeight:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary withWidth:(float)width{
    if(nsDictionary == nil){
        nsDictionary = @{NSFontAttributeName:[UIFont systemFontOfSize:TEXT_SIZE]};
    }
    CGSize cgSize = CGSizeMake(width, 900);
    return [value boundingRectWithSize:cgSize options:NSStringDrawingUsesLineFragmentOrigin attributes:nsDictionary context:nil].size.height;
}

/**
 * @method
 * 一般不会超过900，所以一般而言，这个方法得到的就是不换行整一排过去的高度
 */
+ (float) getNSStringHeight:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary{
    return [CommonUtils getNSStringHeight:value nsDictionary:nsDictionary withWidth:900];
}

+(float) getNSStringHeight:(NSString *)value widthFont:(UIFont *)uiFont{
    NSDictionary *nsDictionary = @{NSFontAttributeName:[UIFont systemFontOfSize:TEXT_SIZE]};
    return [CommonUtils getNSStringHeight:value nsDictionary:nsDictionary withWidth:900];
}

+ (float) getNSStringWidth:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary withHeight:(float)height{
    if(nsDictionary == nil){
        nsDictionary = @{NSFontAttributeName:[UIFont systemFontOfSize:TEXT_SIZE]};
    }
    CGSize cgSize = CGSizeMake(900, height);
    return [value boundingRectWithSize:cgSize options:NSStringDrawingUsesLineFragmentOrigin attributes:nsDictionary context:nil].size.width;
}

+ (float) getNSStringWidth:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary{
    return [CommonUtils getNSStringWidth:value nsDictionary:nsDictionary withHeight:900];
}

+(float) getNSStringWidth:(NSString *)value font:(UIFont *)uiFont{
    NSDictionary *nsDictionary = @{NSFontAttributeName:uiFont};
    return [CommonUtils getNSStringWidth:value nsDictionary:nsDictionary];
}

+(NSString *) getInterString:(NSString *)value{
    return NSLocalizedString(value, value);
}

+(NSMutableDictionary *)jsonToDictionary:(NSString *)jsonString{
    return [CommonUtils dictionaryWithJsonString:jsonString];
}

//解析成字典
+ (NSMutableDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        return nil;
    }
    return dic;
}

/**
 * json转数组
 */
+(NSMutableArray *)jsonToDictNSArray:(NSString *)jsonString{
    return [self nsArrayWithJsonString:jsonString];
}

//解析成数组
+ (NSMutableArray *)nsArrayWithJsonString:(NSString *)jsonString{
    if(jsonString == nil){
        return nil;
    }
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSMutableArray *array = [NSJSONSerialization JSONObjectWithData:jsonData
                                    options:NSJSONReadingMutableContainers
                                      error:&err];
    if(err) {
        return nil;
    }
    return array;
}

/**
 * 把数组解析成NString
 */
+ (NSString *)arrayToJson:(NSArray *)nsArray{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:nsArray options:kNilOptions error:&error];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    return jsonString;
}


+ (NSString *)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingSortedKeys error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

//移除所有子UIView
+ (void)removeAllSonUIView:(UIView *)uiView{
    for(UIView *view in [uiView subviews])
    {
        [view removeFromSuperview];
    }
}

// NSData 转成 Base64 NSString
+(NSString *)nSDataToBase64:(NSData *)nsData{
    NSString *encodedImageStr = [nsData base64EncodedStringWithOptions:0];
    return encodedImageStr;
}

// Base64 还原成 NSData
+(NSData *)base64ToNSData:(NSString *)base64{
    NSData *decodedImageData = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    return decodedImageData;
}

//图片转成Base64
+ (NSString *)uIImageToBase64:(UIImage *)uiImage{
    NSData *data = UIImageJPEGRepresentation(uiImage,1.0f);
    NSString *encodedImageStr = [data base64EncodedStringWithOptions:0];
    return encodedImageStr;
}

//Base64 转图片
+ (UIImage *)base64ToUIImage:(NSString *)base64{
    NSData *decodedImageData = [[NSData alloc] initWithBase64EncodedString:base64 options:0];
    if(decodedImageData == nil){
        return nil;
    }
    UIImage *decodedImage = [UIImage imageWithData:decodedImageData];
    return decodedImage;
}

//获得唯一标识 uuid
+ (NSString *)getUUID{
    NSString *uuid = [[NSUUID UUID] UUIDString];
    return [uuid stringByReplacingOccurrencesOfString:@"-" withString:@""];
}

+ (UIImage *)getImageFromView:(UIView *)view{
    UIGraphicsBeginImageContext(view.bounds.size);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

/**
 * 将字符串转成 GBK 的字节流
 */
+ (NSData *)textToGBKNSData:(NSString *)text{
    
    NSUserDefaults *defaluts = [NSUserDefaults standardUserDefaults];
    NSString *printerCode = [defaluts stringForKey:@"printer_code"];
    
    if(printerCode == nil){
        printerCode = @"GBK";
    }
    
//    NSStringEncoding gbkEncoding = [PrinterCodeModel getNSStringEncoding:printerCode];
//    NSData *data = [text dataUsingEncoding:gbkEncoding];
    NSData *data = [text dataUsingEncoding:CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000)];
    return data;
}

/**
 * 改变图片的尺寸
 */
+(UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize{
    UIGraphicsBeginImageContext(CGSizeMake(reSize.width, reSize.height));
    [image drawInRect:CGRectMake(0, 0, reSize.width, reSize.height)];
    UIImage *reSizeImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return reSizeImage;
}

/**
 * 获得当前版本号
 */
+(NSString *)getCurrentVersion{
    return NSBundle.mainBundle.infoDictionary[@"CFBundleShortVersionString"];
}


/**
 * 判断参数是否是nil
 */
+(BOOL)judgeParameter:(NSString *)string,...{
    va_list args;
    va_start(args, string);
    if (string)
    {
        NSString * otherString;
        while (1)//在循环中遍历
        {
            //依次取得所有参数
            otherString = va_arg(args, NSString *);
            if(otherString == nil){//当最后一个参数为nil的时候跳出循环
                break;
            }else{
                if(otherString == nil || [otherString isEqualToString:@""]){
                    return false;
                }
            }
        }
    }else{
        va_end(args);
        return false;
    }
    va_end(args);
    return true;
}

/**
 * 判断是否为浮点形：
 */
+ (BOOL)isPureFloat:(NSString*)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    float val;
    return[scan scanFloat:&val] && [scan isAtEnd];
}

/**
 * 判断是否是数字
 */
+ (BOOL)isNum:(NSString *)checkedNumString {
    return [CommonUtils isPureFloat:checkedNumString];
}

@end
