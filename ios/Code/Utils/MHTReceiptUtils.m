//
//  MHTReceiptUtils.m
//  MHTReceipt
//
//  Created by Mac on 2018/10/10.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "MHTReceiptUtils.h"
#import "CommonUtils.h"

@implementation MHTReceiptUtils

+(void)sendNotificationRefresh{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UIViewRefresh" object:nil];
}

+(void)sendNotificationChangeIndex:(int) startIndex widthEndIndex:(int) endIndex{
    NSNumber * tempStartIndex = [[NSNumber alloc] initWithInt:startIndex];
    NSNumber * tempEndIndex = [[NSNumber alloc] initWithInt:endIndex];
    NSDictionary *dic = @{@"startIndex":tempStartIndex,@"endIndex":tempEndIndex};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeBaseControlOrder" object:dic];
}

+(void)sendNotificationShowAttribute:(UIView *)uiView{
    NSDictionary *dic = @{@"uiView":uiView};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"showAttributeUIView" object:dic];
}

//不需要关闭了
+(void)sendNotificationDismissShowAttributeUIView{
   // [[NSNotificationCenter defaultCenter] postNotificationName:@"dismissShowAttributeUIView" object:nil];
}

+(void)sendNotificationInitBaseControlWH{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"initBaseControlWH" object:nil];
}

+(void)sendNotificationInitUIScrollView{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshUIScrollView" object:nil];
}

+(void)sendNotificationPositionChange{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"positionChange" object:nil];
}

+(void)sendNotificationSaveModel{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"saveModel" object:nil];
}

+(void)sendNotificationSaveAsModel{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"saveAsModel" object:nil];
}

//打印的通知
+(void)sendNotificationPrintf{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"printf" object:nil];
}

//蓝牙名称改变监听
+(void)sendNotificationPrintfNameChange:(NSString *)printfName{
    NSDictionary *dict = @{@"printfName":printfName};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"printfNameChange" object:dict];
}

/**
 * 打印方式改变
 * typeName = 0 蓝牙连接
 * typeName = 1 wifi连接
 */
+(void)sendNotificationPrintfTypeChange:(NSString *)typeName{
    NSDictionary *dict = @{@"typeName":typeName};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"prinftTypeName" object:dict];
}

//通知去改变打印类型
+(void)sendNotificationChangeType{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"changeType" object:nil];
}

//上传模板的通知
+(void)sendNotificationUploadTemplate{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"uploadTemplate" object:nil];
}

/**
 * 通知打印机的指令模式已经改变：改变发生的情况是，主动去查询打印机发生的
 */
+(void)sendNotificationPrinterInstructChange{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"printerInstructChange" object:nil];
}

/**
 * 通知打印机的纸张模式已经改变：改变发生的情况是，主动去查询打印机发生的
 */
+(void)sendNotificationPrinterPaperChange{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"printerPaperChange" object:nil];
}

/**
 * 登录状态改变
 * 登录 状态通知做不同的通知转发
 * code == 0 登录成功
 * code == 1 登录失败
 */
+(void)sendNotificationLoginStateChange:(NSString *)msg withLoginState:(int)code{
    NSNotificationCenter *defaultCenter = [NSNotificationCenter defaultCenter];
    if(code == 0){//登录成功
        [defaultCenter postNotificationName:@"loginSuccess" object:msg];
    }else if(code == 1){//登录失败
        [defaultCenter postNotificationName:@"loginFail" object:msg];
    }
}

//密码加密
+(NSString *)passwordEncry:(NSString *)password{
    return password;
}

@end
