//
//  CommonUtils.h
//  MHTReceipt
//
//  Created by Mac on 2018/10/9.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CommonUtils : NSObject

+ (BOOL)judgeIsItpPoint:(CGRect)cgRect withCGPoint:(CGPoint)cgPoint;

+ (CGFloat)autoScreen;

+ (float) getNSStringHeight:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary withWidth:(float)width;

+ (float) getNSStringHeight:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary;

+ (float) getNSStringWidth:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary withHeight:(float)height;

+ (float) getNSStringWidth:(NSString *)value nsDictionary:(NSDictionary *)nsDictionary;

+(float) getNSStringHeight:(NSString *)value widthFont:(UIFont *)uiFont;

+ (NSString *) getInterString:(NSString *)value;

+(float) getNSStringWidth:(NSString *)value font:(UIFont *)uiFont;

//解析成字典
+ (NSMutableDictionary *)dictionaryWithJsonString:(NSString *)jsonString;

+(NSMutableDictionary *)jsonToDictionary:(NSString *)jsonString;

//解析成数组
+ (NSMutableArray *)nsArrayWithJsonString:(NSString *)jsonString;

/**
 * json转数组
 */
+(NSMutableArray *)jsonToDictNSArray:(NSString *)jsonString;

//移除所有子View
+ (void)removeAllSonUIView:(UIView *)uiView;

// 字典转化成字符串
+ (NSString*)dictionaryToJson:(NSDictionary *)dic;

// 把数组解析成NString
+ (NSString *)arrayToJson:(NSArray *)nsArray;

//uiImage 转 Base64
+ (NSString *)uIImageToBase64:(UIImage *)uiImage;

//Base64 转 uiImage
+ (UIImage *)base64ToUIImage:(NSString *)base64;

//获得唯一标识 uuid
+ (NSString *)getUUID;

//将UIView转成UIImage
+ (UIImage *)getImageFromView:(UIView *)view;

//将字符串转成GBK 的字节流
+ (NSData *)textToGBKNSData:(NSString *)text;

/**
 * 改变图片的尺寸
 */
+(UIImage *)reSizeImage:(UIImage *)image toSize:(CGSize)reSize;

// NSData 转成 Base64 NSString
+(NSString *)nSDataToBase64:(NSData *)nsData;

// Base64 还原成 NSData
+(NSData *)base64ToNSData:(NSString *)base64;

/**
 * 判断参数是否是nil
 */
+(BOOL)judgeParameter:(NSString *)string,...;

/**
 * 获得当前版本号
 */
+(NSString *)getCurrentVersion;

/**
 * 将字节数组，拼接成16进制的小写字符
 */
+(NSString *)bytesToHexNSString:(NSData *)nsData;

/**
 * 判断是否是数字
 */
+ (BOOL)isNum:(NSString *)checkedNumString;

/**
 * 判断是否为浮点形：
 */
+ (BOOL)isPureFloat:(NSString*)string;

@end

NS_ASSUME_NONNULL_END
