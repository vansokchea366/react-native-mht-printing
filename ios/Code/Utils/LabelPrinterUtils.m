//
//  LabelPrinterUtils.m
//  DemoLabel
//
//  Created by Mac on 2018/12/8.
//  Copyright © 2018年 label printf demo. All rights reserved.
//

#import "LabelPrinterUtils.h"
#import "CommonUtils.h"

@implementation LabelPrinterUtils

/**
 * 把LabelModel 转化成 打印机可以识别的数据
 */
+(NSData *)toLabelNSData:(LabelModel *)labelModel number:(int)number{

    NSMutableData *instructionData = [[NSMutableData alloc] initWithCapacity:5];
    
    NSData *initCanvasNSData = [LabelPrinterUtils initCanvas:labelModel.labelWidth height:labelModel.labelHeight];
    [instructionData appendData:initCanvasNSData];
    
    NSData *clearCanvasNSData = [LabelPrinterUtils clearCanvas];
    [instructionData appendData:clearCanvasNSData];
    
    NSData *printfUIImageNSData = [LabelPrinterUtils printfUIImage:0 y:0 UIImage:labelModel.printfUIImage concentration:128];
    [instructionData appendData:printfUIImageNSData];
    
    NSData *beginPrintfNSData = [LabelPrinterUtils beginPrintf:1 groupNumber:number];
    [instructionData appendData:beginPrintfNSData];
    
    return instructionData;
}

/**
 * begin printf
 */
+ (NSData *)beginPrintf:(int)sequence groupNumber:(int)groupNumber{
    NSString *instruction = [NSString stringWithFormat:@"PRINT %i,%i\r\n",sequence,groupNumber];
    return [CommonUtils textToGBKNSData:instruction];
}

/**
 * 初始化画布
 * Initialization canvas
 */
+ (NSData *)initCanvas:(CGFloat)width height:(CGFloat)height{
    NSMutableString *instructionNSString = [[NSMutableString alloc] initWithCapacity:5];
    [instructionNSString appendString:@"SIZE "];
    [instructionNSString appendString:[NSString stringWithFormat:@"%f",width]];
    [instructionNSString appendString:@" mm,"];
    [instructionNSString appendString:[NSString stringWithFormat:@"%f",height]];
    [instructionNSString appendString:@" mm\r\n"];
    return [CommonUtils textToGBKNSData:instructionNSString];
}

/**
 * 清除画布
 * clear Canvas
 */
+(NSData *)clearCanvas{
    NSString *text = @"CLS\r\n";
    return [CommonUtils textToGBKNSData:text];
}

/**
 * 图片 已经出来过大小的UIImage
 * The UI Image of the size of the picture has come out
 * x: image x coordinate Not to be negative
 * y: image y coordinate Not to be negative
 */
+(NSData *)printfUIImage:(int)x y:(int)y UIImage:(UIImage *)image concentration:(int)concentration{
    
    NSString *instruction = [NSString stringWithFormat: @"BITMAP %i,%i,%f,%f,1,",x,y,image.size.width / 8,image.size.height];
    
    NSMutableData *nsData = [[NSMutableData alloc] initWithCapacity:5];
    [nsData appendData:[CommonUtils textToGBKNSData:instruction]];
    
    NSData *uiimageNSData = [self covertToGrayScaleWithImage:image];
    [nsData appendData:uiimageNSData];

    [nsData appendData:[CommonUtils textToGBKNSData:@"\r\n"]];

    return nsData;
}


/**
 * 图片的二值化。这个方法的意思是，把图片转化成打印机可以识别的数据格式。
 * Binarization of pictures. The idea of this method is to convert pictures into data formats that printers can recognize.
 */
+ (NSData *)covertToGrayScaleWithImage:(UIImage *)image{
    
    int width =image.size.width;
    int height =image.size.height;
    //像素将画在这个数组
    uint32_t *pixels = (uint32_t *)malloc(width *height *sizeof(uint32_t));
    //清空像素数组
    memset(pixels, 0, width*height*sizeof(uint32_t));
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    //用 pixels 创建一个 context
    CGContextRef context = CGBitmapContextCreate(pixels, width, height, 8, width*sizeof(uint32_t), colorSpace, kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedLast);
    CGContextDrawImage(context, CGRectMake(0, 0, width, height), image.CGImage);
    
    int tt =1;
    int bw = 0;
    Byte bytes[width / 8 * height];
    int p[8]={0,0,0,0,0,0,0,0};
    for (int y = 0; y <height; y++) {
        for (int x =0; x <width/8; x ++) {
            for(int z = 0; z < 8; z++){
                uint8_t *rgbaPixel = (uint8_t *)&pixels[(y*width)+(x* 8+z)];
                
                int red = rgbaPixel[tt];
                int green = rgbaPixel[tt + 1];
                int blue = rgbaPixel[tt + 2];
                int gray = 0.29900 * red + 0.58700 * green + 0.11400 * blue; // 灰度转化公式
                if (gray <= 128) {
                    gray = 0;
                } else {
                    gray = 1;
                }
                p[z] = gray;
            }
            int value = (p[0] * 128 + p[1] * 64 + p[2] * 32 + p[3] * 16 + p[4] * 8 + p[5] * 4 + p[6] * 2 + p[7]);
            bytes[bw] = value;
            bw++;
        }
    }
    free(pixels);
    NSData *data = [NSData dataWithBytes: bytes length: sizeof(bytes)];
    return data;
    
}

@end
