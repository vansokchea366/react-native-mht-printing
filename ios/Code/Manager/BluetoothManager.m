//
//  BluetoothManager.m
//  MHTReceipt
//
//  Created by Mac on 2018/11/6.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import "BluetoothManager.h"
// #import "MHTReceiptUtils.h"
// #import "ZFToast.h"
#import "CommonUtils.h"

@interface BluetoothManager()

typedef void (^ReadNSData)(NSData *nsData);

@property (assign,nonatomic)BOOL isHasScanPermisson;

@property (strong,nonatomic)CBCentralManager *manager;

//写入的属性
@property (strong,nonatomic)CBCharacteristic *writeCharacteristic;
//当前的设备
@property (strong,nonatomic)CBPeripheral *currentCBPeripheral;

//读取的闭包
@property (strong,nonatomic) ReadNSData readNSData;

@property (assign,nonatomic) BOOL isCanPrinter;

@end

@implementation BluetoothManager

static BluetoothManager* _instance = nil;

+(instancetype) bluetoothManagerInstance{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[super allocWithZone:NULL] init];
        [_instance initData];
    });
    return _instance;
}

/**
 * 是否可以打印
 */
-(BOOL)isCanPrinter{
    return _isCanPrinter;
}

/**
 * 停止扫描
 //连接成功
 @property (strong,nonatomic)void (^connectSuccess)(void);
 
 @property (strong,nonatomic)void (^scanBlueName)(CBPeripheral *peripheral);
 
 @property (strong,nonatomic)void (^scanStop)(void);

 */
-(void)stopScan{
    [self.manager stopScan];
    self.connectSuccess = nil;
    self.scanStop = nil;
    self.scanBlueName = nil;
}

/**
 * 得到当前连接的蓝牙名称
 */
-(NSString *)getCurrentName{
    if(self.currentCBPeripheral == nil){
        return [CommonUtils getInterString:@"未连接蓝牙"];
    }
    return self.currentCBPeripheral.name;
    
}


//写入函数
-(BOOL)writeNSData:(NSData *)nsData isJudge:(BOOL)isJudge{
    if(self.writeCharacteristic == nil || self.currentCBPeripheral == nil){
        // [[[ZFToast alloc] init] popUpToastWithMessage:[CommonUtils getInterString:@"写入失败，特征值为nil"]];
        return false;
    }
    
    [self.currentCBPeripheral writeValue:nsData forCharacteristic:self.writeCharacteristic type:CBCharacteristicWriteWithResponse];
    
    return true;
}

-(BOOL)writeNSData:(NSData *)nsData{
    return [self writeNSData:nsData isJudge:true];
}

//写入text
-(void)writeText:(NSString *)text{
    //转成GBK
    NSStringEncoding gbkEncoding = CFStringConvertEncodingToNSStringEncoding(kCFStringEncodingGB_18030_2000);
    NSData *data = [text dataUsingEncoding:gbkEncoding];
    [self writeNSData:data];
}

//是否已经连接
-(BOOL)isConnected{
    if(self.currentCBPeripheral == nil || self.writeCharacteristic == nil){
        return false;
    }
    return true;
}



/**
 * 写入 通过block 回调读取结果
 */
-(void)writeAndRead:(NSData *)nsData readNSData:(ReadNSData) readNSData{
    
//    if(self.readNSData != nil){
//        [MHTReceiptUtils ToastText:getInterString(@"读取失败:打印机")];
//        return;
//    }
    if(![self isConnected]){
//        block([[NSDate alloc] init]);
        return;
    }
    self.readNSData = readNSData;
    [self writeNSData:nsData isJudge:false];
    
//    //两秒后，把block变为nil
//    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:2
//                                                  target:self
//                                                selector:@selector(dataNoReturn:)
//                                                userInfo:nil
//                                                 repeats:false];
//    [[NSRunLoop mainRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
}


-(void)initData{
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
    self.isHasScanPermisson = false;
}


-(void)connect:(CBPeripheral *)peripheral{
    if(peripheral.state == CBPeripheralStateConnecting){
        return;
    }
    [self.manager stopScan];
    [self.manager connectPeripheral:peripheral options:nil];
}


-(void)beginScan{
    if(self.manager != nil){
        [self realStartScan];
        return;
    }
    self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
}

//真正扫描的地方
-(void) realStartScan{
    if (self.manager != nil && self.isHasScanPermisson) {
        if (self.manager.isScanning) {
            [self.manager stopScan];
        }
        if(self.currentCBPeripheral != nil){
            [self.manager cancelPeripheralConnection:self.currentCBPeripheral];
            self.currentCBPeripheral = nil; 
        }
        [self.manager scanForPeripheralsWithServices:nil options:@{CBCentralManagerScanOptionAllowDuplicatesKey:@(false)}];
    }else{
        // [[ZFToast alloc] popUpToastWithMessage:[CommonUtils getInterString:@"没有扫描权限或者管理器为nil"]];
    }
}

+(id) allocWithZone:(struct _NSZone *)zone{
    return [BluetoothManager bluetoothManagerInstance];
}

-(id) copyWithZone:(struct _NSZone *)zone{
    return [BluetoothManager bluetoothManagerInstance];
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    
    switch (central.state) {
        case 0:
            break;
        case 1:
            break;
        case 2:
            break;
        case 3:
            break;
        case 4:{
            // [[[ZFToast alloc] init] popUpToastWithMessage:@"手机未开启蓝牙"];
        }
            break;
        case 5:{
            self.isHasScanPermisson = true;
            //self.manager = [[CBCentralManager alloc] initWithDelegate:self queue:dispatch_get_main_queue()];
        }
            break;
        default:
            break;
    }
}

// 发现外设后调用的方法
- (void)centralManager:(CBCentralManager *)central // 中心管理者
 didDiscoverPeripheral:(CBPeripheral *)peripheral // 外设
     advertisementData:(NSDictionary *)advertisementData // 外设携带的数据
                  RSSI:(NSNumber *)RSSI {// 外设发出的蓝牙信号强度
    
    if(peripheral == nil || peripheral.name == nil || [peripheral.name isEqualToString:@""]){
        return;
    }
    
    if (![self.peripheralsArray containsObject:peripheral] ) {
        [self.peripheralsArray addObject:peripheral];
    }
    
    if(self.scanBlueName != nil){
        self.scanBlueName(peripheral);
    }
    
}

// 中心管理者连接外设成功
- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral {

    self.currentCBPeripheral = peripheral;
    [peripheral discoverServices:nil];
    peripheral.delegate = self;
    
    NSString *uuid = peripheral.identifier.UUIDString;
    
    [[NSUserDefaults standardUserDefaults] setObject:uuid forKey:@"bluetooth_uuid"];
    
    // [MHTReceiptUtils sendNotificationPrintfNameChange:peripheral.name];
    
    if(self.connectSuccess != nil){
        self.connectSuccess();
    }
}

//发现服务
- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    NSArray<CBService *> * services = peripheral.services;
    for(int i = 0; i <services.count; i++){
        [peripheral discoverCharacteristics:nil forService:services[i]];
    }
}

//得到返回的值
- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if (self.readNSData != nil) {
        self.readNSData(characteristic.value);
    }
}

//断开连接
- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    self.currentCBPeripheral = nil;
    self.writeCharacteristic = nil;
    
    // [MHTReceiptUtils sendNotificationPrintfNameChange:@"未连接"];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    NSArray<CBCharacteristic *> *characteristics = service.characteristics;
    for(int i = 0; i < characteristics.count;i++){
        CBCharacteristic *characteristic = characteristics[i];
        if(characteristic.properties & CBCharacteristicPropertyNotify){
            [peripheral setNotifyValue:true forCharacteristic:characteristic];
        }
        
        if(self.writeCharacteristic == nil && characteristic.properties & CBCharacteristicPropertyWrite){
            self.writeCharacteristic = characteristic;
        }
    }
}


- (NSMutableArray *)peripheralsArray{
    if (!_peripheralsArray) {
        _peripheralsArray = [NSMutableArray array];
    }
    return _peripheralsArray;
}

@end
