//
//  BluetoothManager.h
//  MHTReceipt
//
//  Created by Mac on 2018/11/6.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BluetoothModel.h"
#import "PrintfManager.h"
#import <CoreBluetooth/CoreBluetooth.h>

NS_ASSUME_NONNULL_BEGIN

@interface BluetoothManager : PrintfManager<CBCentralManagerDelegate,CBPeripheralDelegate>

+(instancetype) bluetoothManagerInstance;

@property (strong,nonatomic)NSMutableArray *peripheralsArray;

-(void)connect:(CBPeripheral *)peripheral;

-(void)beginScan;

//连接成功
@property (strong,nonatomic)void (^connectSuccess)(void);

@property (strong,nonatomic)void (^scanBlueName)(CBPeripheral *peripheral);

@property (strong,nonatomic)void (^scanStop)(void);


/**
 * 得到当前连接的蓝牙名称
 */
-(NSString *)getCurrentName;

/**
 * 停止扫描
 */
-(void)stopScan;

/**
 * 是否可以打印
 */
-(BOOL)isCanPrinter;

@end

NS_ASSUME_NONNULL_END
