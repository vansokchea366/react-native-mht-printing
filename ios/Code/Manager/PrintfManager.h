//
//  PrintfManager.h
//  MHTReceipt
//
//  Created by Mac on 2018/11/6.
//  Copyright © 2018年 Mac. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface PrintfManager : NSObject


//是否已经连接
-(BOOL)isConnected;

//写入函数
-(BOOL)writeNSData:(NSData *)nsData;

//写入text
-(void)writeText:(NSString *)text;

/**
 * 写入 通过block 回调读取结果
 */
-(void)writeAndRead:(NSDate *)nsData block:(void(^)(NSDate *nsDate)) block;

@end

NS_ASSUME_NONNULL_END
