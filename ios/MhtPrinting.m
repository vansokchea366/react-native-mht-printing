#import "MhtPrinting.h"
#import "BluetoothModel.h"
#import "BluetoothManager.h"
#import <CoreBluetooth/CoreBluetooth.h>
#import "CommonUtils.h"
#import "LabelModel.h"
#import "LabelPrinterUtils.h"

#define NO_SCANNING 0
#define SCANNING 1

@interface MhtPrinting ()

@property (assign,nonatomic) int type;

//集合
@property (strong,nonatomic) NSMutableArray<BluetoothModel *>* bluetoothModels;

//蓝牙管理器
@property (strong,nonatomic) BluetoothManager *bluetoothManager;

@end

@implementation MhtPrinting

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.type = NO_SCANNING;
        
        self.bluetoothModels = [NSMutableArray arrayWithCapacity:5];
        self.bluetoothManager = [BluetoothManager bluetoothManagerInstance];
        
//        [self initBlock];
    }
    return self;
}

-(void)initBlock{
     __weak typeof(self) weakSelf = self;
    self.bluetoothManager.scanBlueName = ^(CBPeripheral * _Nonnull peripheral) {
        NSString *uuid = peripheral.identifier.UUIDString;

        for(int i = 0; i < weakSelf.bluetoothModels.count;i++){
            BluetoothModel *bluetoothModel = weakSelf.bluetoothModels[i];
            NSString *tempUUID = bluetoothModel.peripheral.identifier.UUIDString;
            if([uuid isEqualToString:tempUUID]){
                return;
            }
        }
        
        BluetoothModel *bluetoothModel = [[BluetoothModel alloc] init];
        bluetoothModel.peripheral = peripheral;
        [weakSelf.bluetoothModels addObject:bluetoothModel];
        
        if (hasListeners) { // Only send events if anyone is listening
            [weakSelf sendEventWithName:@"onBluetoothDeviceFound" body:@{@"name": peripheral.name, @"address": uuid}];
        }
    };
    
    self.bluetoothManager.connectSuccess = ^{
        if (hasListeners) { // Only send events if anyone is listening
            [weakSelf sendEventWithName:@"onBluetoothDeviceConnected" body:@{}];
        }
    };
}

- (void)startObserving {
    hasListeners = YES;
}

-(void)stopObserving {
    hasListeners = NO;
}

- (NSArray<NSString *> *)supportedEvents {
    return @[@"onBluetoothDeviceFound", @"onBluetoothDeviceConnected"];
}

+(void)attach {
    [BluetoothManager bluetoothManagerInstance];
}

-(void)beginScan{
    [self.bluetoothModels removeAllObjects];
    [self.bluetoothManager beginScan];
}

+ (BOOL)requiresMainQueueSetup {
    return true;
}

- (dispatch_queue_t)methodQueue {
    return dispatch_get_main_queue();
}

RCT_EXPORT_MODULE(MHTPrinting)

RCT_EXPORT_METHOD(stopSearchBluetoothDevices) {
    [self.bluetoothManager stopScan];
    NSLog(@"Stop scanning...");
}

RCT_EXPORT_METHOD(getConnectedBluetoothDevice:(RCTResponseSenderBlock)callback) {
    BluetoothModel *connectedBluetoothModel = nil;
    NSString *deviceName = nil;
    NSString *deviceAddress = nil;
    
    for(int i = 0;i < self.bluetoothModels.count;i++) {
        BluetoothModel *bluetoothModel = self.bluetoothModels[i];
        if (bluetoothModel.peripheral.state == CBPeripheralStateConnected) {
            connectedBluetoothModel = bluetoothModel;
            break;
        }
    }
    
    if (connectedBluetoothModel != nil) {
        deviceName = connectedBluetoothModel.peripheral.name;
        deviceAddress = [connectedBluetoothModel.peripheral.identifier UUIDString];
    }
    
    if (deviceName == nil) {
        deviceName = @"";
    }
    
    if (deviceAddress == nil) {
        deviceAddress = @"";
    }
    
    if (callback != nil) {
        callback(@[deviceName, deviceAddress]);
    }
}

RCT_EXPORT_METHOD(getPairedBluetoothDevices:(RCTPromiseResolveBlock)resolve
                  withRejecter:(RCTPromiseRejectBlock)reject) {
    resolve(@[]);
}

RCT_EXPORT_METHOD(searchBluetoothDevices) {
//    if(self.type == SCANNING){
//        self.type = NO_SCANNING;
//        [self.bluetoothManager scanStop];
//    }else if(self.type == NO_SCANNING){
//        self.type = SCANNING;
//        [self beginScan];
//    }
//    [self.bluetoothManager scanStop];
    
    [self initBlock];
    [self beginScan];
    NSLog(@"Begin scanning...");
}

RCT_EXPORT_METHOD(connectBluetoothDevice:(NSString*)address) {
    for(int i = 0;i < self.bluetoothModels.count;i++) {
        BluetoothModel *bluetoothModel = self.bluetoothModels[i];
        
        if ([bluetoothModel.peripheral.identifier.UUIDString isEqualToString:address]) {
            [self.bluetoothManager connect:bluetoothModel.peripheral];
            return;
        }
    }
}

RCT_EXPORT_METHOD(connectPairedBluetoothDevice:(NSNumber*)index) {
    int i = [index intValue];
    
    if (i >=0 && i < self.bluetoothModels.count) {
        BluetoothModel *bluetoothModel = self.bluetoothModels[i];
        [self.bluetoothManager connect:bluetoothModel.peripheral];
    }
}

RCT_EXPORT_METHOD(print:(NSString*)base64ImageData
                  options:(NSDictionary*)options
                  withResolver:(RCTPromiseResolveBlock)resolve
                  withRejecter:(RCTPromiseRejectBlock)reject) {
    BluetoothManager *blueToothManager = [BluetoothManager bluetoothManagerInstance];
    if(![blueToothManager isConnected]){
        reject(@"Error", @"NoBluetoothDeviceConnected", nil);
        return;
    }
    
    UIImage *uiImage = [CommonUtils base64ToUIImage:base64ImageData];
    
    CGFloat w = 48;
    CGFloat h = 50;
    
    if (options != nil) {
        NSNumber *x = [options valueForKey:@"w"];
        NSNumber *y = [options valueForKey:@"y"];
        NSNumber *width = [options valueForKey:@"width"];
        NSNumber *height = [options valueForKey:@"height"];
        
        w = [width floatValue];
        h = [height floatValue];
    }
    
    //更换图片的尺寸
    uiImage = [CommonUtils reSizeImage:uiImage toSize:CGSizeMake(w * 8, h * 8)];
    LabelModel *labelModel = [[LabelModel alloc] init];
    labelModel.labelHeight = h;
    labelModel.labelWidth = w;
    labelModel.printfUIImage = uiImage;
    NSData *nsData = [LabelPrinterUtils toLabelNSData:labelModel number:1];
    
    if([blueToothManager writeNSData:nsData]){
        resolve(@[@"Printing"]);
    }
}

@end
