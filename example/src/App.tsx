import * as React from 'react';
import { StyleSheet, View, Button, Platform } from 'react-native';
import {
  PrinterConnectionModal,
  printerConnectionModalRef,
  tryPrint,
  DefaultPrintingOptions,
} from 'react-native-mht-printing';
import { launchImageLibrary } from 'react-native-image-picker';
import {
  check as checkPermission,
  request as requestPermission,
  PERMISSIONS,
  RESULTS,
} from 'react-native-permissions';

export default function App() {
  const pickImage = () => {
    launchImageLibrary(
      {
        quality: 0.4,
        includeBase64: true,
        mediaType: 'photo',
      },
      (response) => {
        if (!response.didCancel && !response.errorMessage) {
          let asset = response.assets[0];
          let data =
            Platform.OS === 'ios'
              ? `data:${asset.type};base64,${asset.base64}`
              : asset.base64;

          tryPrint(data ?? '', {
            printingCanvas: DefaultPrintingOptions,
          });
        }
      }
    );
  };

  const print = async () => {
    const locationPermission =
      Platform.OS === 'android'
        ? PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION
        : PERMISSIONS.IOS.LOCATION_WHEN_IN_USE;
    let result = await checkPermission(locationPermission);

    if (result == RESULTS.GRANTED) {
      pickImage();
    } else if (result == RESULTS.DENIED) {
      const result = await requestPermission(locationPermission);

      if (result == RESULTS.GRANTED) {
        pickImage();
      }
    }
  };

  const connectPrinter = () => {
    printerConnectionModalRef.current?.show(undefined);
  };

  return (
    <View style={styles.container}>
      <Button title="PRINT" onPress={print} />
      <View style={styles.divider} />
      <Button title="CONNECT PRINTER" onPress={connectPrinter} />
      <PrinterConnectionModal
        ref={printerConnectionModalRef}
        closeButtonTitle="Close"
        pairedSectionTitle="Paired Devices"
        unpairedSectionTitle={Platform.select({
          android: 'Unpaired Devices',
          ios: 'Devices',
          default: '',
        })}
        connectedSectionTitle="Connected Device"
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  divider: {
    height: 10,
  },
});
